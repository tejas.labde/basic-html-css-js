let firstCard;
let secondCard;
let hasBlackJack=false;
let isAlive=false;
let message="";
let cardsArray=[];

let messageText=document.getElementById('welcomeMessage');
let listOfCards=document.getElementById('cardList');
let playerElement=document.getElementById('playerElement');
let sumOfCards=document.getElementById('sumOfCards');
let sum = 0;
let playerName="Per";
let playerChips=145;
playerElement.textContent= playerName +': $' +playerChips;
console.log()

function startGame(){
    isAlive=true;
    firstCard=getRandomCard();
    secondCard=getRandomCard();
    
    // cardsArray=[firstCard,secondCard];
    // sum+=firstCard+secondCard;
    renderGame();
   
}

function renderGame(){
    if(sum<=20){
        message="Do you want to draw a new card?";
      
        
    }
    else if(sum === 21){
        message='Wohooo!You got blackjack';
        hasBlackJack=true;
        
        }
    else {
        message="You're out of the game";
        isAlive=false;
        
    }
    sumOfCards.textContent=" Sum: " +sum;
        listOfCards.textContent="Cards: "
        for(let counter=0;counter<cardsArray.length;counter++){
           listOfCards.textContent += cardsArray[counter] +", ";
    messageText.textContent=message;
}
}


function newCard(){
    if(isAlive && hasBlackJack===false){
        const newCardNumber=getRandomCard();
    sum+=newCardNumber;
    cardsArray.push(newCardNumber);
    startGame();
    }
    

}

function getRandomCard(){
    let randomNumber=Math.floor(Math.random()*13)+1;
    if(randomNumber == 1){
        return 11
    }
    else if(randomNumber >10 ){
        return 10;
    }
    else{
        return randomNumber;
    }
 
}

