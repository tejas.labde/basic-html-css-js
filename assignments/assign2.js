const numbers=[10,12,13,15,17,20,21];
const answerArray=[];
for(let index=0; index<numbers.length;index++){
    if(numbers[index]%2==0){
        answerArray.push(numbers[index]);
    }
}

console.log(answerArray) ;


//custom reduce function to find sum of all numbers of an array.

Array.prototype.customReduce=function(finalValue){
    let sum=0;
    for(let element of this){
        sum=finalValue(element);
    }
    return sum;
}


[12,34,14,10,8].customReduce(x=> x+=x);

Array.prototype.customReduce = function(cb , initialValue) {
    let value;
    if(!initialValue){
        value=this[0];
        for(let element =1;element<this.length;element++) {
            value = cb(value,this[element] );
        }
    }
   else{
    for(let element of this) {
        value = cb(value, element);
    }
   }
    return value;
}

//changed code
Array.prototype.customReduce = function(cb , initialValue) {
    let value=initialValue,index=0;
    if(!initialValue){
        value=this[0];
         index=1;
    }
    

    for(index;index<this.length;index++) {
            value = cb(value,this[index] );
        }
    
  
   
    return value;
}

