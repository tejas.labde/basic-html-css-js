const images=document.getElementById('imgs');
const leftBtn=document.getElementById('left');
const rightBtn=document.getElementById('right');

const img = document.querySelectorAll('#imgs img');

let indexEl=0;

let interval= setInterval(run,2000)

function run(){
    indexEl++;
    changeImage()
}

function changeImage(){
    if(indexEl>img.length-1){
        indexEl=0;
    }else if(indexEl<0){
        indexEl=img.length-1;
    }

    images.style.transform= `translateX(${-indexEl *20}em)`;
}


function resetInterval(){
    clearInterval(interval)
    interval=setInterval(run,2000);

}

rightBtn.addEventListener('click',()=>{
    indexEl++;
    changeImage();
    resetInterval();
})

leftBtn.addEventListener('click',()=>{
    indexEl--;
    changeImage();
    resetInterval();
})