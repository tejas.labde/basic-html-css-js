console.log('js is working');

const addBtn=document.getElementById('add')

addBtn.addEventListener('click',()=> addNewNote())

function addNewNote(text=''){
    const note=document.createElement('div')
    note.classList.add('note')
    note.innerHTML= `
    <div class="note">
            <div class="tools">
                <button class="edit btn"><i class="fas fa-edit"></i></button>
                    <button class="delete btn"><i class="fas fa-trash akt"></i></button>
            </div>

            <div class="main ${text? "" : "hidden"}"></div>
            <textarea class="${text? "hidden":""}"></textarea>
    </div>
    `
    document.body.appendChild(note);
}